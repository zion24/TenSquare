"""TenSq URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.urls import register_converter

# 注册自定义转换器
from apps.users.utils import UsernameConverter,MobileConverter
register_converter(UsernameConverter,'username')
register_converter(MobileConverter,'mobile')

from django.urls import register_converter
from apps.users import utils
register_converter(utils.UsernameConverter, 'username')
register_converter(utils.MobileConverter, 'mobile')

urlpatterns = [
    path('admin/', admin.site.urls),
    path('ckeditor/', include('apps.headlines.urls')),
    # 用户模块
    path('', include('apps.users.urls')),

    # 吐槽模块
    path('', include('apps.talks.urls')),

    # 招聘模块
    path('', include('apps.recruits.urls')),

    # 问答模块
    path('', include('apps.answers.urls')),

    # 文章模块
    path("", include("apps.headlines.urls")),

    # 活动模块
    path('', include('apps.activitys.urls')),

    # 头条模块
    path('', include('apps.headlines.urls')),

]
