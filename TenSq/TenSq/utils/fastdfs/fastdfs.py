from django.core.files.storage import Storage
from django.conf import settings

class FastDFSStorage(Storage):

    def __init__(self,base_url=None):
        # 这是从配置文件找 --http://image.meiduo.site:8888/
        self.base_url = base_url or settings.FDFS_BASE_URL

    def _open(self, name,mode='rb'):
        pass

    def _save(self, name, content):
        pass

    def url(self,name):
        """
               返回name所指文件的绝对URL
               :param name: 要读取文件的引用:group1/M00/00/00/wKhnnlxw_gmAcoWmAAEXU5wmjPs35.jpeg
               :return: http://192.168.103.158:8888/group1/M00/00/00/wKhnnlxw_gmAcoWmAAEXU5wmjPs35.jpeg
               """
        # return 'http://192.168.103.158:8888/' + name
        # return 'http://image.meiduo.site:8888/' + name
        return self.base_url + name