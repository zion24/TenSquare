from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse


class LoginRequiredJSONMixin(LoginRequiredMixin):

    # 重写  父类 的 未登录的函数
    def handle_no_permission(self):
        # 将父类的HttpResponseRedirect 替换成 JsonResponse
        return JsonResponse({'code': 400, 'errmsg': '未登录!'})