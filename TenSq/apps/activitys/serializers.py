from rest_framework import serializers
from apps.activitys.models import Gathering

class GatheringsSerializer(serializers.ModelSerializer):

    users = serializers.PrimaryKeyRelatedField(many=True,read_only=True)
    class Meta:
        model = Gathering
        fields = "__all__"


class GatherSerializerSimple(serializers.ModelSerializer):

    class Meta:
        model = Gathering
        fields = ['id','name','image','city','starttime','endrolltime','users']