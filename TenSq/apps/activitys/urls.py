from django.urls import path
from . import views

urlpatterns = [
    # 获取活动列表
    path('gatherings/', views.GatheringsModelViewSet.as_view()),
    # 获取活动详情
    path('gatherings/<int:pk>/', views.DetailsAPIView.as_view()),
    # 参加或取消活动
    path('gatherings/<int:pk>/join/', views.GatherJoinView.as_view()),
]
