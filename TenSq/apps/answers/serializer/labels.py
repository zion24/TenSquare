from rest_framework import serializers

from apps.answers.models import Label, Question
from apps.headlines.models import Article
from apps.users.models import User


class LabelsSerializer(serializers.ModelSerializer):
    '''标签序列化器'''

    class Meta:
        model = Label
        fields = ['id', 'label_name']
        extra_kwargs = {
            'label_name': {
                'read_only': True
            }
        }


class ArticleUserArticleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Article
        fields = ('id', 'title')


class ArticleUserSerializer(serializers.ModelSerializer):
    articles = serializers.SerializerMethodField()
    fans = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ('id', 'username', 'avatar', 'articles', 'fans')

    def get_articles(self, obj):
        data = obj.articles.all()
        return ArticleUserArticleSerializer(data, many=True).data

    def get_fans(self, obj):
        data = obj.fans.all()
        fans = []
        for i in data:
            fans.append(i.id)
        return fans


class LabelQuestionSerializer(serializers.ModelSerializer):
    labels = serializers.SerializerMethodField()

    class Meta:
        model = Question
        fields = '__all__'

    def get_labels(self, obj):
        data = obj.labels.all()
        labels = []
        for i in data:
            labels.append(i.label_name)
        return labels


class ArticlesSerializer(serializers.ModelSerializer):
    user = ArticleUserSerializer()
    collected_users = serializers.SerializerMethodField()

    class Meta:
        model = Article
        exclude = ('thumbup', 'updatetime', 'comment_count', 'channel', 'labels')

    def get_user(self, obj):
        data = obj.users.all()
        return ArticleUserSerializer(data, many=True).data

    def get_collected_users(self, obj):
        data = obj.collected_users.all()
        collected_users = []
        for i in data:
            collected_users.append(i.id)
        return collected_users


class LabelsDetailSerializer(serializers.ModelSerializer):
    questions = serializers.SerializerMethodField()
    users = serializers.SerializerMethodField()
    articles = serializers.SerializerMethodField()

    class Meta:
        model = Label
        fields = '__all__'

    def get_questions(self, obj):
        data = obj.questions.all()
        return LabelQuestionSerializer(data, many=True).data

    def get_users(self, obj):
        data = obj.users.all()
        users = []
        for i in data:
            users.append(i.id)
        return users

    def get_articles(self, obj):
        data = obj.articles.all()
        return ArticlesSerializer(data, many=True).data


class LabelFullSerializer(serializers.ModelSerializer):
    users = serializers.SerializerMethodField()

    class Meta:
        model = Label
        fields = '__all__'

    def users(self, obj):
        data = obj.users.all()
        users = []
        for i in data:
            users.append(i.id)
        return users
