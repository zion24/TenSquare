from rest_framework import serializers

from apps.answers.models import Question, Reply
from apps.users.models import User


# 用户
class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'avatar')


# 问题答案
class ReplySerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)

    class Meta:
        model = Reply
        fields = ('id', 'content', 'createtime', 'useful_count', 'unuseful_count', 'user')


class QuestionModelSerializerForList(serializers.ModelSerializer):
    # GET问答的序列化器
    user = serializers.StringRelatedField(read_only=True)
    labels = serializers.StringRelatedField(read_only=True, many=True)

    class Meta():
        model = Question
        fields = ["id", "createtime", "labels", "reply", "replyname", "replytime", "title", "unuseful_count",
                  "useful_count", "user", "visits"]


class QuestionModelSerializerForCreate(serializers.ModelSerializer):
    # 发布get的序列化器

    class Meta():
        model = Question
        fields = "__all__"


# 问题评论
class ReplyCommentSerializer(serializers.ModelSerializer):
    subs = serializers.SerializerMethodField(read_only=True)
    user = UserSerializer(read_only=True)

    class Meta:
        model = Reply
        fields = ('id', 'content', 'createtime', 'useful_count', 'problem', 'unuseful_count', 'subs', 'user', 'parent')

    def get_subs(self, obj):
        data = obj.subs.all()
        subs = []
        for i in data:
            subs.append(i.content)
        return subs


# 问题答案
class ReplyAnswerSerializer(serializers.ModelSerializer):
    subs = serializers.SerializerMethodField(read_only=True)
    user = UserSerializer()

    class Meta:
        model = Reply
        fields = ('id', 'content', 'createtime', 'useful_count', 'problem', 'unuseful_count', 'subs', 'user', 'parent')

    def get_subs(self, obj):
        data = Reply.objects.filter(parent=obj)
        return ReplySerializer(data, many=True).data


#  问题详情
class QuestionsSerializer(serializers.ModelSerializer):
    labels = serializers.SerializerMethodField()
    comment_question = serializers.SerializerMethodField()
    answer_question = serializers.SerializerMethodField()
    user = serializers.CharField()

    class Meta:
        model = Question
        fields = ('id', 'createtime', 'labels', 'reply', 'replyname', 'replytime', 'title',
                  'useful_count', 'unuseful_count', 'user', 'visits', 'content', 'comment_question', 'answer_question')

    def get_labels(self, obj):
        data = obj.labels.all()
        labels = []
        for i in data:
            labels.append(i.label_name)
        return labels

    def get_comment_question(self, obj):
        comments = obj.replies.filter(type=0)
        return ReplyCommentSerializer(comments, many=True).data

    def get_answer_question(self, obj):
        answers = obj.replies.filter(type=2)
        return ReplyAnswerSerializer(answers, many=True).data


class QuestionsPostSerializer(serializers.ModelSerializer):
    """发布问题"""
    labels = serializers.SerializerMethodField()
    user = serializers.StringRelatedField()

    class Meta:
        model = Question
        fields = '__all__'

    def get_labels(self, obj):
        data = obj.labels.all()
        labels = []
        for i in data:
            labels.append(i.label_name)
        return labels
