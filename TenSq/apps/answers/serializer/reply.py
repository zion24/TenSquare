from rest_framework import serializers
from apps.answers.models import Reply


#  回答问题
class ReplyQuestionSerializer(serializers.ModelSerializer):
    # problem = serializers.PrimaryKeyRelatedField()
    # parent = serializers.PrimaryKeyRelatedField()
    user_id = serializers.IntegerField()
    class Meta:
        model = Reply
        fields = '__all__'


class ReplySerializerForCreate(serializers.ModelSerializer):

    class Meta():
        model = Reply
        fields = "__all__"
