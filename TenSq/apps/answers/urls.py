from django.urls import path
from rest_framework.routers import SimpleRouter

from . import views


urlpatterns = [

]

router = SimpleRouter()

# 标签
router.register(r'labels', views.LabelsModelView, basename='labels')
# 最新,热门,等待问题
# router.register(r'questions/(?P<id>-?\d+)/label', views.QuestionViewSet)
#
router.register(r'questions', views.QuestionViewSet)

router.register(r'reply', views.ReplyViewSet)

urlpatterns += router.urls

