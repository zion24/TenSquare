from django.conf import settings
from django.core.files.storage import Storage


class FastDFSStorage(Storage):
    def __init__(self, base_url=None):
        # 这是从 配置文件 找 --http://image.meiduo.site:8888/
        self.base_url = base_url or settings.FDFS_BASE_URL

    def _open(self, name, mode='rb'):
        pass

    def _save(self, name, content, max_length=None):
        pass

    def url(self, name):

        # 自定义 ImageField 返回的 全路由
        # name group1/M00/00/00/wKhnnlxw_gmAcoWmAAEXU5wmjPs35.jpeg

        return self.base_url + name
