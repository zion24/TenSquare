from rest_framework import serializers

from apps.answers.models import Label
from apps.headlines.models import Article, Comment
from apps.users.models import User


class ArticleAdd(serializers.ModelSerializer):
    """发布文章序列化器"""
    image = serializers.CharField(required=False, default="", allow_blank=True)

    class Meta:
        model = Article
        exclude = ("collected_users",)


class ArticleSerializer(serializers.ModelSerializer):
    """用户判断序列化器"""

    class Meta:
        model = Article
        fields = ("id", "title")


class UserSerializer(serializers.ModelSerializer):
    """用户序列化器"""
    article = ArticleSerializer(read_only=True, many=True)

    class Meta:
        model = User
        fields = ("id", "username", "avatar", "article", "fans")


class UserSerializerDetail(serializers.ModelSerializer):
    """获取用户发布序列化器"""
    username = serializers.StringRelatedField()
    articles = serializers.StringRelatedField(many=True)

    class Meta:
        model = User
        fields = "__all__"


class ArticleListSerializer(serializers.ModelSerializer):
    """文章列表序列化器"""
    collected = serializers.BooleanField(default=False)

    class Meta:
        model = Article
        fields = ("collected", "collected_users", "content", "createtime", "id", "image", "title", "user", "visits")


class SearchForArticleSerializer(serializers.ModelSerializer):
    """搜索文章列表序列化器"""

    class Meta:
        model = Article
        fields = "__all__"


class ArticleCollectSerializer(serializers.ModelSerializer):
    '''文章收藏/取消收藏序列化器'''
    class Meta:
        model = Article
        fields = '__all__'


class CommentPublicSerializer(serializers.ModelSerializer):
    '''发布评论序列化器'''
    class Meta:
        model = Comment
        fields= "__all__"


class CommentSerializerItem(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)
    subs = serializers.PrimaryKeyRelatedField(read_only=True, many=True)

    class Meta:
        model = Comment
        fields = ('id', 'content','article','user','parent','subs','createtime')


class CommentTreeSerializerList(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)

    class Meta:
        model = Comment
        fields = '__all__'


class CommentTwoSerializerList(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)
    subs = serializers.SerializerMethodField()

    class Meta:
        model = Comment
        fields = '__all__'

    def get_subs(self, obj):
        subs = Comment.objects.filter(parent=obj)
        return CommentTreeSerializerList(subs, many=True).data


class CommentSerializerList(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)
    subs = serializers.SerializerMethodField()

    class Meta:
        model = Comment
        fields = '__all__'

    def get_subs(self, obj):
        subs = Comment.objects.filter(parent=obj)
        return CommentTwoSerializerList(subs, many=True).data


class ArticleDetailSerializer(serializers.ModelSerializer):
    """文章详情序列化器"""
    user = UserSerializer(read_only=True)
    labels = serializers.SerializerMethodField()
    collected_users = serializers.SerializerMethodField()

    class Meta:
        model = Article
        fields = "__all__"

    # 获取标签
    def get_labels(self, obj):
        # 获取label数据
        data = obj.labels.all()
        # labels = Label.objects.all()
        # 创建一个空列表用于存放获取的标签
        labels_list = []
        # 遍历获取的数据
        for i in data:
            labels_list.append(i.id)
        return labels_list

    # 获取所有用户
    def get_collected_users(self, obj):
        # # 获取数据
        # users = User.objects.filter(id=1)
        # users_id = []
        #
        # for i in users:
        #     users_id.append(i.username)
        # return users_id

        data = obj.collected_users.all()
        # users = User.objects.filter(id=1)
        users_list = []
        for i in data:
            users_list.append(i.username)
        return users_list

    def get_comments(self, obj):
        comments = Comment.objects.filter(article=obj).filter(parent=None)
        return CommentSerializerList(comments, many=True).data


class CommentSerializer(serializers.ModelSerializer):
    parent_id = serializers.IntegerField()
    class Meta:
        model = Comment
        fields = ['article','content','parent']

    def create(self, validated_data):
        # 保存数据
        comment = Comment.objects.create(**validated_data)

        return comment