from rest_framework import serializers

from apps.answers.models import Label
from apps.headlines.models import Channel


class ChannelList(serializers.ModelSerializer):
    """获取频道列表"""
    class Meta:
        model = Channel
        fields = "__all__"
