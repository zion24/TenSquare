from rest_framework import serializers

from apps.headlines.models import Article


class ImageSerializer(serializers.ModelSerializer):
    """文章图片序列化器"""
    class Meta:
        model = Article
        fields = ("image",)