from django.urls import path

from apps.headlines.views.articlelist import SearchForArticle
from .views import channellist,articlelist,image
from . import views

urlpatterns = [
    # 频道列表
    path("channels/", channellist.ChannelListView.as_view()),
    # 搜索
    path("articles/search/", SearchForArticle.as_view()),
]

from rest_framework.routers import DefaultRouter

# 创建router实例
router = DefaultRouter()
# 注册路由，basename(别名)
router.register(r'article', articlelist.ArticleListView, basename='article')
# 拼接路由，添加到urlpatterns
urlpatterns += router.urls


# 创建router实例(image路由)
router = DefaultRouter()
# 注册路由，basename(别名)
router.register(r'upload/avatar', image.ArticleImage, basename='avatar')
# 拼接路由，添加到urlpatterns
urlpatterns += router.urls
