from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response


class PageNum(PageNumberPagination):
    """自定义分页类"""

    # 后端指定每页显示数量（如果不传递这个数字才会生效），相当于一个默认值
    page_size = 2
    # 传给前端的关键字 www.xxx.com/?page=xxx&....
    page_size_query_param = 'page'
    # 每页最大显示数量
    max_page_size = 2

    # 重写分页返回方法，按照我们需要的字段进行分页将数据返回
    def get_paginated_response(self, data):

        # 自定义我们需要的格式
        return Response({
            "count":self.page.paginator.count,  # 总数量
            "lists":data,  # 用户数据
            "page":self.page.number,  # 当前页数
            "pages":self.page.paginator.num_pages,  # 总页数
            "pagesize":self.page_size  # 后端指定的页容量
        })
