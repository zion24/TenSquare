from rest_framework.response import Response
from rest_framework.views import APIView

from apps.headlines.models import Channel
from apps.headlines.serializers.channellist import ChannelList


class ChannelListView(APIView):
    """获取频道列表"""
    def get(self,request):
        # 查询数据
        channelname = Channel.objects.all()
        # 创建序列化器，并传递查询结果集
        serializer = ChannelList(channelname,many=True)
        # 返回响应
        return Response({
            "count":6,
            "next":"http://52.83.143.113:8880/channels/?page=2",
            "previous":"null",
            "results":serializer.data
        })
