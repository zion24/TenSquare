from rest_framework import serializers

from .models import Enterprise,Recruit,City

# 热门城市
class CitySerializers(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = ['id','name','ishot']

# 公司
class EnterpriseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Enterprise
        fields = ['id','name','labels','logo','summary','coordinate']


# 职位
class RecruitSerializer(serializers.ModelSerializer):
    enterprise = EnterpriseSerializer(label='企业')
    class Meta:
        model = Recruit
        fields = ['id','jobname','salary','condition','education','type','city','address'
                  ,'createtime','enterprise','labels']


# 公司详情
class EnterpriseSer(serializers.ModelSerializer):
    recruits = RecruitSerializer(label='职业信息',many=True)
    class Meta:
        model = Enterprise
        fields = '__all__'

#
class Recruits(serializers.ModelSerializer):
    class Meta:
        model = Recruit
        fields = ['id']
# 热门公司
class HotEnterpriseSer(serializers.ModelSerializer):
    recruits = Recruits(label='职业ID',many=True)
    class Meta:
        model = Enterprise
        fields = ['id','labels','logo','name','recruits','summary']



# 职位详情
class RecruitSer(serializers.ModelSerializer):
    enterprise = EnterpriseSer(label='企业详情')
    class Meta:
        model = Recruit
        fields = '__all__'

