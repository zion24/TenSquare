from django.urls import path
from . import views
from rest_framework import routers

urlpatterns = [
    # 热门城市
    path('city/hotlist/', views.HotCityListView.as_view()),

    # 最新职位
    path('recruits/search/latest/',views.LatestRecruitAPIView.as_view()),

    # 推荐职位
    # path('recruits/search/recommend/',views.RecruitListView.as_view()),

    # 职位详情
    path('recruits/<int:id>/',views.RecruitView.as_view()),

    # 增加职位访问次数
    path('recruits/<int:id>/visit/',views.VisitRecruitView.as_view()),

    # 收藏职位
    path('recruits/<int:id>/collect/',views.CollectRecruits.as_view()),

    # 取消收藏职位
    path('recruits/<int:id>/cancelcollect/',views.CancelCollectRecruit.as_view()),

    #搜索职位
    path('recruits/search/city/keyword/',views.SearchRecruit.as_view()),

    #推荐职位
    path('recruits/search/recommend/',views.RecruitListView.as_view()),

    # 企业详情
    path('enterprise/<int:id>/',views.EnterpriseView.as_view()),

    # 热门企业
    path('enterprise/search/hotlist/',views.HotEnterpriseView.as_view()),

    # 增加企业访问次数
    path('enterprise/<int:id>/visit/',views.VisitEnterpriseView.as_view()),

    # 收藏企业
    path('enterprise/<int:id>/collect/',views.CollectEnterprise.as_view()),

    # 取消收藏企业
    path('enterprise/<int:id>/cancelcollect/',views.CancelCollectEnterprise.as_view()),

]









