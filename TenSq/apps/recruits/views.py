from django.shortcuts import render
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import action
from rest_framework.views import APIView
from rest_framework.response import Response

from .serializers import CitySerializers,RecruitSerializer,RecruitSer
from .serializers import EnterpriseSer,EnterpriseSerializer,HotEnterpriseSer
from .models import City,Recruit,Enterprise
from apps.users.models import User
import json

from TenSq.utils.view import LoginRequiredJSONMixin


# 热门城市
class HotCityListView(APIView):

    def get(self, requset):
        city = City.objects.filter(ishot=1)

        serializer = CitySerializers(city, many=True)

        return Response(serializer.data)



# 最新职位
class LatestRecruitAPIView(APIView):

    def get(self, request):
        recruits = Recruit.objects.filter().order_by('-id')[0:4]

        serializer = RecruitSerializer(recruits, many=True)

        return Response(serializer.data)

# # 推荐职位
class RecruitListView(APIView):

    def get(self,request):
        recruits = Recruit.objects.filter().order_by('-id')[0:4]

        serializer = RecruitSerializer(recruits,many=True)
        return Response(serializer.data)



# 职位详情
class RecruitView(APIView):

    def get(self,request,id):
        recruit = Recruit.objects.get(id=id)
        serializer = RecruitSer(recruit)
        return Response(serializer.data)

# 企业详情
class EnterpriseView(APIView):

    def get(self,request,id):
        enterprise = Enterprise.objects.get(id=id)
        serializer = EnterpriseSer(enterprise)
        return Response(serializer.data)

# 热门企业
class HotEnterpriseView(APIView):
    def get(self,request):
        hotenterprise = Enterprise.objects.filter().order_by('-visits')[0:5]
        serializer = HotEnterpriseSer(hotenterprise,many=True)
        return Response(serializer.data)

# 增加职位访问次数
class VisitRecruitView(APIView):
    def put(self,request,id):
        recruit = Recruit.objects.get(id=id)
        recruit.visits += 1
        recruit.save()
        return Response({"message":"更新成功","success":True})

# 增加企业访问次数
class VisitEnterpriseView(APIView):
    def put(self,request,id):
        enterprise = Enterprise.objects.get(id=id)
        enterprise.visits += 1
        enterprise.save()
        return Response({"message": "更新成功", "success": True})


# 收藏职业
class CollectRecruits(APIView):
    @action(methods=['post'], detail=True)
    def post(self,request,id):
        # 判断是否登录
        if not request.user.is_authenticated:
            return Response({"message": "未登录", "success": False})
        recruit = Recruit.objects.get(id=id)
        user = User.objects.get(id=request.user.id)
        # user = User.objects.get(id=1)
        recruit.users.add(user)
        recruit.save()
        return Response({"message": "收藏成功", "success": True})

# 取消收藏职位
class CancelCollectRecruit(APIView):
    @action(methods=['post'], detail=True)
    def post(self, request, id):
        # 判断是否登录
        if not request.user.is_authenticated:
            return Response({"message": "未登录", "success": False})
        recruit = Recruit.objects.get(id=id)

        user = User.objects.get(id=request.user.id)
        # user = User.objects.get(id=1)
        recruit.users.remove(user)

        recruit.save()

        return Response({"message": "取消收藏","success": True})

# 收藏企业
class CollectEnterprise(APIView):
    @action(methods=['post'], detail=True)
    def post(self,request,id):
        # 判断是否登录
        if not request.user.is_authenticated:
            return Response({"message": "未登录", "success": False})
        enterprise = Enterprise.objects.get(id=id)
        user = User.objects.get(id=request.user.id)
        # user = User.objects.get(id=2)
        enterprise.users.add(user)
        enterprise.save()
        return Response({"message": "收藏成功", "success": True})

# 取消收藏企业
class CancelCollectEnterprise(APIView):
    @action(methods=['post'], detail=True)
    def post(self,request,id):
        # 判断是否登录
        if not request.user.is_authenticated:
            return Response({"message": "未登录", "success": False})

        enterprise = Enterprise.objects.get(id=id)
        user = User.objects.get(id=request.user.id)
        # user = User.objects.get(id=2)
        enterprise.users.add(user)
        enterprise.save()
        return Response({'message':'取消收藏','success':True})


# 搜索职位
class SearchRecruit(APIView):
    def post(self,request):
        # 接收参数
        json_dict = json.loads(request.body.decode())
        city = json_dict.get('cityname')
        keyword = json_dict.get('keyword')
        if city and keyword:
            recruits = Recruit.objects.filter(city = city,jobname__contains = keyword)
            serializer = RecruitSerializer(recruits,many=True)
            return Response(serializer.data)
        elif city:
            recruits = Recruit.objects.filter(city=city)
            serializer = RecruitSerializer(recruits,many=True)
            return Response(serializer.data)
        elif keyword:
            recruits = Recruit.objects.filter(jobname__contains=keyword)
            serializer = RecruitSerializer(recruits,many=True)
            return Response(serializer.data)










