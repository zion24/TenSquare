from rest_framework import serializers
from .models import Spit


class SpitReleaseModelSerializer(serializers.ModelSerializer):
    parent = serializers.IntegerField(label="被吐槽的吐槽")

    class Meta:
        model = Spit
        fields = ['id', 'content', 'publishtime', 'userid', 'nickname', 'visits',
                  'thumbup', 'comment', 'avatar', 'parent', 'collected', 'hasthumbup']


class SpitListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Spit
        fields = ['id', 'content', 'publishtime', 'userid', 'nickname', 'visits',
                  'thumbup', 'comment', 'avatar', 'parent_id', 'collected', 'hasthumbup']
