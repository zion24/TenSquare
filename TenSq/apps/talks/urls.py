from django.urls import path
from . import views

urlpatterns = [
    # 吐槽列表和发布吐槽
    path('spit/', views.SpitReleaseView.as_view()),

    # 吐槽详情
    path('spit/<int:pk>/',views.SpitDetailsView.as_view()),

    # 获取吐槽评论
    path('spit/<int:pk>/children/',views.SpitParentView.as_view()),

    # 收藏或取消收藏
    path('spit/<int:pk>/collect/',views.CollectView.as_view()),

    # 点赞或取消点赞
    path('spit/<int:pk>/updatethumbup/',views.LikeView.as_view()),
]
