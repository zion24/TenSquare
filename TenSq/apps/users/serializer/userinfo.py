from rest_framework import serializers
from rest_framework.serializers import ModelSerializer

from apps.answers.models import Reply, Question, Label
from apps.headlines.models import Article
from apps.recruits.models import Recruit, Enterprise
from apps.talks.models import Spit
from apps.users.models import User


# from .models import Article
# from .models import User
# from .models import Label, Reply, Question
# from .models import Enterprise, Recruit
# from .models import Spit

# 用户中心序列化器


class ReplySerializer(ModelSerializer):
    '''回复规格表序列化器'''

    user = serializers.PrimaryKeyRelatedField(read_only=True)
    content = serializers.StringRelatedField(read_only=True)

    class Meta:
        model = Reply
        # fields = ("id", "content", "createtime", "useful_count", 'problem', "unuseful_count", "user")
        fields = ("id", "content", "createtime", 'problem', "user", 'useful_count', 'type')


class QuestionSerializer(ModelSerializer):
    ''' Question规格表序列化器'''

    # user = serializers.CharField(read_only=True)

    class Meta:
        model = Question
        fields = '__all__'

        def __str__(self):
            return self.user


class RecruitSerializer(ModelSerializer):
    class Meta:
        model = Recruit
        fields = '__all__'


class LabelsSerializer(ModelSerializer):
    '''Label规格表序列化器'''

    # user = QuestionSerializer(read_only=True)

    class Meta:
        model = Label
        fields = '__all__'


class ArticleSerializer(ModelSerializer):
    '''文章阅读规格表序列化器'''
    user = serializers.CharField(read_only=True)

    class Meta:
        model = Article
        fields = '__all__'


class EnterpriseSerializer(ModelSerializer):
    '''企业规格表序列化器'''

    # user = QuestionSerializer(read_only=True)
    # recruits = RecruitSerializer(read_only=True)

    class Meta:
        model = Enterprise
        fields = ('id', 'name', 'labels', 'logo', 'recruits', 'summary')


class SpitSerializer(ModelSerializer):
    '''吐槽序列化器'''

    class Meta:
        model = Spit
        fields = '__all__'


class UserSerializer(ModelSerializer):
    '''用户序列化器'''

    answer_question = serializers.SerializerMethodField()
    username = serializers.CharField(read_only=True)
    questions = QuestionSerializer(many=True)
    labels = LabelsSerializer(many=True)
    collected_articles = serializers.SerializerMethodField()
    enterpises = EnterpriseSerializer(many=True)

    # collectSpits = SpitSerializer(many=True)

    class Meta:
        model = User
        # fields = '__all__'
        fields = '__all__'

    def get_answer_question(self, obj):
        reply = Reply.objects.filter(user=obj)
        return ReplySerializer(reply, many=True).data

    def get_collected_articles(self, obj):
        articles = Article.objects.filter(user=obj)
        return ArticleSerializer(articles, many=True).data


class LikeSerializer(ModelSerializer):
    fans = serializers.PrimaryKeyRelatedField(required=True, many=True, queryset=User.objects.all())

    class Meta:
        model = User
        fields = ('id', 'fans')


# 修改擅长技术
class UpdateLabelSerializer(serializers.ModelSerializer):
    # labels = serializers.PrimaryKeyRelatedField(required=True, many=True, queryset=Label.objects.all())

    class Meta:
        model = User
        fields = '__all__'
