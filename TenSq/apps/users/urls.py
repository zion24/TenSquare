from django.urls import path,re_path
from rest_framework_jwt.views import obtain_jwt_token

from . import views

urlpatterns = [
    # path('', views.IndexView.as_view()),
    path('users/', views.RegisterView.as_view()),  # 注册子路由
    # path('authorizations/', views.LoginView.as_view()),  # 登录子路由

    path('authorizations/', obtain_jwt_token),

    path('sms_codes/<mobile:mobile>/', views.SMSCodeView.as_view()),  # 短信验证码子路由

    path('user/', views.PersonListView.as_view()),
    re_path(r'^user/password/$', views.PasswordView.as_view()),
    re_path(r'^upload/avatar/$', views.AvatarView.as_view()),
    # re_path(r'^upload/common/$', views.CommonView.as_view()),
    re_path(r'^users/like/(?P<pk>\d+)/$', views.LikeView.as_view()),
]
