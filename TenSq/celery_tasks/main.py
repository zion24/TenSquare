# 1.导包
from celery import Celery

import os
if not os.getenv('DJANGO_SETTINGS_MODULE'):
    os.environ['DJANGO_SETTINGS_MODULE'] = 'meiduo_mall.settings.dev'


# 2.实例化
celery_app = Celery('meiduo')

# 3.加载消息对列的配置
celery_app.config_from_object('celery_tasks.config')

# 4.自动加载 任务
celery_app.autodiscover_tasks(['celery_tasks.sms','celery_tasks.email'])

