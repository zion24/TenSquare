from celery_tasks.yuntongxun.ccp_sms import CCP
from celery_tasks.main import celery_app

# 定义 发短信的 功能函数
@celery_app.task(name="ccp_send_sms_code")
def ccp_send_sms_code(mobile, sms_code):
    #                     手机号, ['验证码', 过期时间], 短信模板1
    result = CCP().send_template_sms(mobile, [sms_code, 5], 1)
    print(result)

    return result